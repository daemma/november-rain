//  ############################################################################
//! @file       functions.js
//! @brief      Functions for calculating Poisson values
//! @author     "Emma Hague" <dev@daemma.io>
//! @date       2017-11-22
//! @copyright  Copyright (C) 2017 "Emma Hague" <dev@daemma.io>
//!             GPLv3+: See License.txt or <https://www.gnu.org/licenses/gpl.txt>
var dist = require('distributions.io');

function calcpVal(form) {
    var k   = Math.floor(eval(form.k.value));
    var lam = eval(form.lambda.value);
    return 1 - dist.poisson.cdf(k, {'lambda': lam});
}

function calcPercpVal(form) {
    var val = calcpVal(form);
    form.percpVal.value = val * 100.;
}

function calcLog10pVal(form) {
    var val = calcpVal(form);
    form.log10pVal.value = -Math.log(val)/Math.LN10;
}

//  end functions.js
//  ############################################################################


// document.getElementById("demo").innerHTML  =
// 	"This is minus the base-10 logarithm of the probability of observing";
// document.getElementById("demo").innerHTML += " k=" + k;
// document.getElementById("demo").innerHTML +=
// 	" or greater, counts given that the true distribution is a Poisson with parameter";
// document.getElementById("demo").innerHTML += " &lambda;=" + lam + ".";


// function formSum() {
//     var x = document.getElementById("frm1");

//     // var sum = 0;
//     // var i;
//     // for (i = 0; i < x.length ;i++) {
//     // 	sum += parseFloat(x.elements[i].value);
//     // }
//     // document.getElementById("demo").innerHTML = "Sum = " + sum;

//     var lambda = parseFloat(x.elements[1].value);
//     var k      = parseFloat(x.elements[0].value);
//     var cdf    = dist.poisson.cdf(k, {'lambda': lambda});
//     var pval   = 1 - cdf;
    
//     document.getElementById("demo").innerHTML  = "&lambda; = " + lambda;
//     document.getElementById("demo").innerHTML += ", k = " + k;
//     document.getElementById("demo").innerHTML += ", CDF = " + cdf;
//     document.getElementById("demo").innerHTML += ", Pval = " + pval;
// }
