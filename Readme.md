# november-pain

> We will rain down Poisson statistics for Novembers and Novembers!

Version 0.0.2

## Dev

```
$ npm install
```

### Run

```
$ npm start
```

### Build

```
$ npm run build
```

Builds the app for macOS, Linux, and Windows, using 
[electron-packager](https://github.com/electron-userland/electron-packager).


## License

[GNU Puclic License v3+](https://www.gnu.org/licenses/gpl-3.0.en.html)
